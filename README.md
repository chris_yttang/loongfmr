# README #
**To get all case studies' source code and experimental data described in our paper please use following link:** [https://drive.google.com/open?id=0B9l0qvk6pnW0SEdCdE1taTNjVGc](https://drive.google.com/open?id=0B9l0qvk6pnW0SEdCdE1taTNjVGc)

**To report bugs, please write an email to LoongFMRPlugin**: loongplugin@gmail.com

**The source code of other tools are available**
**Bunch**[ https://www.cs.drexel.edu/~spiros/bunch/](https://www.cs.drexel.edu/~spiros/bunch/)
**ACDC**[http://www.cs.yorku.ca/~bil/downloads/](http://www.cs.yorku.ca/~bil/downloads/)
**ARC**[https://drive.google.com/file/d/0BwAC4jK9s_7eZTZpV29La0ozV0U/view?usp=sharing](https://drive.google.com/file/d/0BwAC4jK9s_7eZTZpV29La0ozV0U/view?usp=sharing)
In ARC source code, you can also find the implementation of LIMBO, W-UE, W-UENM.


**A full feature list of Berkeley DB and relations are available at **  [https://docs.google.com/document/d/1FM4yRgdSXiIov5fQDPGm0y7tTjtiLk9poWV8GK5ZSVw/edit?usp=sharing](https://docs.google.com/document/d/1FM4yRgdSXiIov5fQDPGm0y7tTjtiLk9poWV8GK5ZSVw/edit?usp=sharingL)